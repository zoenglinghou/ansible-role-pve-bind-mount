pve_bind_mount
=========

Setup bind mount between Proxmox PVE host and guest LXC.

Requirements
------------

- `community.general.lxc_container` collection
- `proxmoxer` Python module on PVE host

Role Variables
--------------

⚠️ Note: A lot of defaults **NEED** to be changed.

- `node`
  - `host`: Ansible host corresponding to the Proxmox host machine. Default: `proxmox`
  - `hostname`: Hostname of the Proxmox system. Default: `proxmox`
  - `share_dir`: Directory to share with the guest VM on the host machine. Default: `/mnt/data`
  - `user`: Username in the host OS to be mapped to the LXC. Default: `guest`
  - `group`: Group name in the host OS to be mapped to the LXC. Default: `guest`
- `guest`
  - `host`: Ansible host corresponding to the guest VM. Default: `guest`
  - `hostname`: Hostname of the VM in Proxmox. Default: `guest`
  - `id`: VM ID. Default: `101`
  - `disk`: id of the disk for the container. Default: `0`
  - `mount_point`: Mount point of the file share inside the VM. Default: `/mnt/share`
  - `user`: Username of the directory of the owner inside the LXC. Default: `guest`
  - `group`: Group name of the directory of the owner inside the LXC. Default: `guest`
- `api_user`: Username to login Proxmox. Default: `root`
- `api_password`: Password for the corresponding Proxmox user. Default: `api_password`
- `api_token_id`: Token ID to log into Proxmox. Default: `api_token_id`
- `api_token_secret`: Token Secret for the corresponding Token. Default: `api_token_secret`

Example Playbook
----------------

```yaml
- hosts: pve_node, pve_guest
  roles:
  - name: zoenglinghou.pve_bind_mount
    vars:
      node:
        host: pve_node
        hostname: pve_node
        share_dir: /mnt/data/pve_guest
        user: guest
        group: guest
      guest:
        host: pve_guest
        hostname: pve_guest
        id: 101
        disk: 0
        mount_point: /mnt/share
        user: guest
        group: guest
      api_user: root@pam
      api_password: password
      api_token_id: ansible_0
      api_token_secret: xxx-xxx-xxx
```

License
-------

GPL-3.0-only
